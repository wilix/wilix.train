'use strict';

const express = require('express');
const http = require('http');
const path = require('path');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const app = express();

// настройка для системы шаблонизации
app.set('views', __dirname + '/views');
app.set('view engine', 'html');
app.engine('html', require('ejs').renderFile);
//body-parser
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: false}));
// parse application/json
app.use(bodyParser.json());

//middleware
app.get('/', (req, res, next) => {
    res.render('index');
});
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'views')));

// схемы и модели
let userSchema = mongoose.Schema({
    login: String,
    password: String
});
let User = mongoose.model('User', userSchema);


let boardSchema = mongoose.Schema({
    title: String,
    id:[{
        type: Schema.Types.ObjectId,
        ref: 'User'
    }]
});
let Board = mongoose.model('Board', boardSchema);


let cardSchema = mongoose.Schema({
    title: String,
    content: String,
    board_id:[{
        type: Schema.Types.ObjectId,
        ref: 'Board'
    }]
});
let Card = mongoose.model('Card', cardSchema);


// ----------------------------------------------------------------- USER start
app.get('/reg', (req, res) => {
    res.render('reg', {title: 'testTitle'});
});

app.post('/reg', (req, res) => {
    User.findOne({login: req.body.login}, (err, user) => {
        if(user == null){
            let user = new User({
                login: req.body.login,
                password: req.body.password
            });
            user.save((err, item) => {
                if (err) return console.error(err);
                res.send('user saved');
            });
        }else if(user.login == req.body.login){
            res.send('user exist');
        }else{
            res.send(err);
        }




    });
});

// ----------------------------------------------------------------- USER end

           //  БОРДЫ
app.get('/get_all_boards', (req, res) => {
    Board.find({}, (err, Board) => {
        res.send(Board);
    });
});

app.post('/get_one_board', (req, res) => {
    let boardid = req.body.id;
    Board.findById({_id: boardid}, (err, board) => {
        res.send(board);
    });
});

app.post('/delete_board', (req, res) => {
    let id = req.body.id;
    Board.remove({_id: id}, () => {
        res.sendStatus(200);
    });
});

app.post('/add_board', (req, res) => {
    var board = new Board({
        title: req.body.title,
        id : req.body.id
    });
    board.save(function(err, item){
        if (err) return console.error(err);
        res.send(item);
    });
});

app.post('/edit_board', (req, res) =>{
    Board.findOne({_id: req.body.id}, (err, board) => {
        board.title = req.body.title;
        board.save(function (err, item) {
            if (err) return handleError(err);
            res.send(item);
        })
    });
});


         //  КАРТОЧКИ
app.get('/get_all_cards/:id', (req, res) => {
    let boardid = req.params.id;
    Card.find({board_id: boardid}, (err, Card) => {
        res.send(Card);
    });
});

app.post('/add_card', (req, res) => {
    var card = new Card({
        title: req.body.title,
        content : req.body.content,
        board_id : req.body.id
    });
    card.save(function(err, item){
        if (err) return console.error(err);
        res.send(item);
    });
});

app.post('/delete_card', (req, res) => {
    Card.remove({_id: req.body.id}, ()  => {
        res.sendStatus(200)
    });
});

app.post('/edit_card', (req, res) => {
    Card.findOne({board_id: req.body.board_id}, (err, card) => {
        card.title = req.body.title;
        card.content = req.body.content;
        card.board_id = req.body.board_id;
        card.save((err) => {
           if (err) return handleErrer(err);
            res.sendStatus(200)
        });
    });
});

//сервер
app.listen(4000, () => {
    console.log('Cлушаем порт 4000');
    mongoose.connect('mongodb://localhost/to-do');
    let db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
});
