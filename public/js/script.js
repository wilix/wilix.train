$(document).ready(function() {
    let currentCardId = '';

//---------------борды-------------------

    $('#add-board').on('click', function (e) {
    });

    $.get('/get_all_boards', function (data) {
        for (let i = 0; i < data.length; i++) {
            $("#menu").append("<div class='task' id=" + data[i]._id + ">" + data[i].title +
                "</div>" + "<Button id='remove-board' rem-id=\"" + data[i]._id + "\" type='button' class='btn btn-default btn-xs glyphicon glyphicon-remove'></Button>" +
                "<button id='edit-board' edit-id=\"" + data[i]._id + "\" title=\"" + data[i].title + "\" type='button' class='btn btn-default btn-xs glyphicon glyphicon-pencil' data-toggle='modal' data-target='#editBoardModal'> </button>");

        }
    });

    $('#submitBoard').on('click', function (e) {
        let newBoardTitle = $('#addboard').val();
        $.post("/add_board", {title: newBoardTitle},
            function (board) {
                $.get('/get_all_boards', function (data) {
                    $("#menu").empty();
                    for (let i = 0; i < data.length; i++) {
                        $("#menu").append("<div class='task'  id=" + data[i]._id + ">" + data[i].title + "</div>" +
                            "<Button id='remove-board' rem-id=\"" + data[i]._id + "\" type='button' class='btn btn-default btn-xs glyphicon glyphicon-remove'></Button>" +
                            "<button id='edit-board' edit-id=\"" + data[i]._id + "\" title=\"" + data[i].title + "\" type='button' class='btn btn-default btn-xs glyphicon glyphicon-pencil' data-toggle='modal' data-target='#editBoardModal'></button>");
                    }
                    $('.close').trigger('click');
                });
                $('#addboard').val('');
            }
        );
    });

    $("#menu").on("click", '.task', function (e) {
        let boardid = $(this).attr('id');
        $(".show-block").attr('board-id', boardid);
        renderTable();
    });

    function renderTable() {
        let boardid = $(".show-block").attr('board-id');
        $(".show-block").html('');
        $.post('/get_one_board', {id: boardid}, function (data) {
            if ($(".show-block").find("div").length) {
                $(".show-block").find("div").text(data.title);
            } else {
                $(".hide-block").hide();
                $(".show-block").show();
                $(".show-block").append("<div>" + data.title + "</div><table id='table' class='table table-bordered text-shadow5'><thead><tr><th>#</th><th>Название</th><th>Контент</th><th>Действие</th></tr></thead><tbody  class='todo-table text-shadow6'></tbody></table><button  id=\"card-add\" type=\"button\" class=\"btn btn-success center-block\" data-toggle=\"modal\" data-target=\"#addCardModal\">Добавить карточку</button>");
                $.get('/get_all_cards/' + boardid, function (data) {
                    for (let i = 0; i < data.length; i++) {
                        $("#table").append(
                            "<tr idcard=\"" + data[i]._id + "\">" +
                            "<th scope=\"row\">" + (i + 1) + "</th>" +
                            "<td>" + data[i].title + "</td>" +
                            "<td>" + data[i].content + "</td>" +
                            "<td colspan=\"2\"><button cardid=\"" + data[i]._id + "\" id=\"remove-card\" type=\"button\" class=\"btn btn-danger\">Удалить</button><button title=\"" + data[i].title + "\" content=\"" + data[i].content + "\"  cardid=\"" + data[i]._id + "\" id=\"edit-card\" type=\"button\" class=\"btn btn-warning\" data-toggle=\"modal\" data-target=\"#editCardModal\">Изменить</button></td>" +
                            "</tr>"
                        );
                    }
                });
            }
        });
    }

    $("#menu").on('click', '#remove-board', function (e) {
        let ThisId = $(this).attr("rem-id");
        $.post("/delete_board", {id: ThisId}, function (data) {
            window.location.reload();
        });
    });

    $("#menu").on("click", '#edit-board', function (e) {
        $('#edit-board-title').val(this.title);
        let editById = $(this).attr('edit-id');
        $("#submit-edit").on('click', function (e) {
            let newBoardTitle = $('#edit-board-title').val();
            $.post("/edit_board", {id: editById, title: newBoardTitle},
                function () {
                    window.location.reload();
                }
            );
            $('#edit-board-title').val('');
        });
    });

//------------------карточки---------------------


    $(document).on('click', '#remove-card', function (e) {
        $.post("/delete_card",
            {id: e.target.attributes["cardid"].value}, function (data) {
                renderTable();
            });
    });

    $('#card-add').on('click', function (e) {

    });

    $('#save-card-add-button').on('click', function (e) {
        let idboard = $(".show-block").attr('board-id');
        let newCardTitle = $('#add-card-title').val();
        let newCardContent = $('#add-card-content').val();
        $.post("/add_card", {id: idboard, title: newCardTitle, content: newCardContent},
            function () {
                $('#add-card-title').val('');
                $('#add-card-content').val('');
                renderTable();
            });
        $('.close').trigger('click');
    });

    $(document).on('click', '#edit-card', function (e) {
        currentCardId = e.target.attributes["cardid"].value;
        $('#edit-card-title').val(this.title);
        $('#edit-card-content').val(e.target.attributes["content"].value);
    });

    $('#save-card-edit-button').on('click', function (e) {
        let newCardTitle = $('#edit-card-title').val();
        let newCardContent = $('#edit-card-content').val();
        $.post("/edit_card", {id: currentCardId, title: newCardTitle, content: newCardContent},
            function () {
                renderTable();
            }
        );
        $('.close').trigger('click');
    });

    
});
