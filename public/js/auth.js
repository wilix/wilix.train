$(document).ready(function() {
    $('#sign-account').on('click', function (e) {
        e.preventDefault();
        let newUserLogin = $('#inputLoginSign').val();
        let newUserPassword = $('#inputPasswordSign').val();
        if(newUserLogin == ""){
            $('#error_block').html('Вы не заполнили поле login!');
            return false;
        }
        if(newUserPassword == ""){
            $('#error_block').html('Вы не заполнили поле password!');
            return false;
        }
        $.post("/reg", {login: newUserLogin, password: newUserPassword},
            function (data) {
                var size_numbers = $(".size_numbers");
                $.each(size_numbers, function() {
                    $(this).css("border","red");
                });
                $('#inputLoginSign').val('');
                $('#inputPasswordSign').val('');
               console.log(data);
        });
    });
});