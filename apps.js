'use strict'

const express = require('express');
const http = require('http');
const path = require('path');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const app = express();

// настройка для системы шаблонизации
app.set('views', __dirname + '/views');

//body-parser
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: false}));
// parse application/json
app.use(bodyParser.json());

//middleware
app.get('/', (req, res, next) => {
    res.sendFile(__dirname + '/views/index.html');
});
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'views')));


// схемы и модели
let cardSchema = mongoose.Schema({
    title: String,
    content: String
});

let Card = mongoose.model('Card', cardSchema);

let cardInitData = [
    {
        title: 'zdefbh',
        content: 'fgnn'
    },
    {
        title: 'sdg',
        content: 'tjh'
    }
];
function fileModel(model, data) {
    model.remove({}, () => {
        for (let i in data) {
            new model(data[i]).save((err, item) => {
                if (err) return console.error(err);
            });
        }
    });
}

app.get('/get_all_cards', (req, res) => {
    Card.find({}, (err, cards) => {
        res.send(cards);
    });
});

app.post('/delete_card', (req, res) => {
    console.log('delete_card');
    console.log(req.body);
    Card.remove({_id: req.body.id}, () => {
        res.send(200);
    });
});

app.post('/add_card', (req, res) => {
    console.log('add_card');
    console.log(req.body);
    new Card(req.body).save((err, item) => {
        if (err) return console.error(err);
        res.send(200);
    });
});

app.post('/edit_card', (req, res) => {
    console.log(req.body);
    Card.findOne({_id:req.body.id}, (err, card) => {
        card.title = req.body.title;
        card.content = req.body.content;
        card.save(function (err) {
            if (err) return handleError(err);
            res.sendStatus(200);
        })
    });
});

//сервер
app.listen(3000, () => {
    console.log('Cлушаем порт 3000');
    mongoose.connect('mongodb://localhost/to-do');
    let db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', () => {
        fileModel(Card, cardInitData);
    });
});
